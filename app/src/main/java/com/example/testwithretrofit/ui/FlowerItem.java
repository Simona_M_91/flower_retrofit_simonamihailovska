package com.example.testwithretrofit.ui;

public class FlowerItem {
    int imageView;
    int title;

    public FlowerItem(){}

    public int getImageView() {
        return imageView;
    }

    public void setImageView(int imageView) {
        this.imageView = imageView;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }
}

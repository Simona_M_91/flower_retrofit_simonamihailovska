package com.example.testwithretrofit.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testwithretrofit.R;
import com.example.testwithretrofit.model.adapter.FlowerItemAdapter;
import com.example.testwithretrofit.ui.FlowerItem;

import java.util.ArrayList;
import java.util.List;

public class SecondFragment extends Fragment implements FlowerItemAdapter.ItemClickListener {
    public static final String TAG = SecondFragment.class.getSimpleName();
    RecyclerView recyclerView;
    FlowerItemAdapter flowerItemAdapter;

    ArrayList<FlowerItem> items=new ArrayList<>();
    List<String> text;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.second_fragment, container, false);

        recyclerView = view.findViewById(R.id.recycler_view_facts);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL , false));
        applyItems();

        flowerItemAdapter = new FlowerItemAdapter(items, inflater, this);
        recyclerView.setAdapter(flowerItemAdapter);


        return view;
    }

    private void applyItems() {

        FlowerItem item1 = new FlowerItem();
        item1.setImageView(R.drawable.ten);
        item1.setTitle(R.string.number_10);
        items.add(item1);

        FlowerItem item2 = new FlowerItem();
        item2.setImageView(R.drawable.nine);
        item2.setTitle(R.string.number_9);
        items.add(item2);

        FlowerItem item3 = new FlowerItem();
        item3.setImageView(R.drawable.eight);
        item3.setTitle(R.string.number_8);
        items.add(item3);

        FlowerItem item4 = new FlowerItem();
        item4.setImageView(R.drawable.seven);
        item4.setTitle(R.string.number_7);
        items.add(item4);

        FlowerItem item5 = new FlowerItem();
        item5.setImageView(R.drawable.six);
        item5.setTitle(R.string.number_6);
        items.add(item5);

        FlowerItem item6 = new FlowerItem();
        item6.setImageView(R.drawable.five);
        item6.setTitle(R.string.number_5);
        items.add(item6);

        FlowerItem item7 = new FlowerItem();
        item7.setImageView(R.drawable.four);
        item7.setTitle(R.string.number_4);
        items.add(item7);

        FlowerItem item8 = new FlowerItem();
        item8.setImageView(R.drawable.threee);
        item8.setTitle(R.string.number_3);
        items.add(item8);

        FlowerItem item9 = new FlowerItem();
        item9.setImageView(R.drawable.two);
        item9.setTitle(R.string.number_2);
        items.add(item9);

        FlowerItem item10 = new FlowerItem();
        item10.setImageView(R.drawable.one);
        item10.setTitle(R.string.number_1);
        items.add(item10);

    }


    @Override
    public void onItemClick(View view, int position) {

    }

}

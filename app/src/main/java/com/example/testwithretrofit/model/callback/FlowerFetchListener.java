package com.example.testwithretrofit.model.callback;

import com.example.testwithretrofit.model.pojo.Flower;
import android.content.Context;

import java.util.List;

public interface FlowerFetchListener {
    void onDeliverAllFlowers(List<Flower> flowers);

    void onDeliverFlower(Flower flower);

    void onHideDialog();

}

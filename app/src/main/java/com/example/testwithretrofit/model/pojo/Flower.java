package com.example.testwithretrofit.model.pojo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.gson.annotations.Expose;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

public class Flower implements Serializable {

    private static final long serialVersionUID = 111696345129311948L;
    public byte[] imageByteArray;

    @Expose
    private String category;

    @Expose
    private double price;

    @Expose
    private String instructions;

    @Expose
    private String photo;

    @Expose
    private String name;

    @Expose
    private int productId;

    private Bitmap picture;

    private boolean isFromDatabase;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public boolean isFromDatabase() {
        return isFromDatabase;
    }

    public void setFromDatabase(boolean fromDatabase) {
        isFromDatabase = fromDatabase;
    }


    private void writeObject(java.io.ObjectOutputStream out) throws IOException {

        out.writeObject(category);
        out.writeObject(price);
        out.writeObject(instructions);
        out.writeObject(photo);
        out.writeObject(name);
        out.writeObject(productId);

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.PNG;
        /*
        public void processPicture(Bitmap bitmap, int encodingType) {
    ByteArrayOutputStream jpeg_data = new ByteArrayOutputStream();
    CompressFormat compressFormat = encodingType == JPEG ?
            CompressFormat.JPEG :
            CompressFormat.PNG;

    try {
        if (bitmap.compress(compressFormat, mQuality, jpeg_data)) {
            byte[] code = jpeg_data.toByteArray();
            byte[] output = Base64.encode(code, Base64.NO_WRAP);
            String js_out = new String(output);
            this.callbackContext.success(js_out);
            js_out = null;
            output = null;
            code = null;
        }
    } catch (Exception e) {
        this.failPicture("Error compressing image.");
    }
    jpeg_data = null;
         */
        picture.compress(compressFormat, 0, byteStream);
        byte [] bitmapBytes = byteStream.toByteArray();
        out.write(bitmapBytes, 0, bitmapBytes.length);
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {

        category = (String) in.readObject();
        price = (Double) in.readObject();
        instructions = (String) in.readObject();
        photo = (String) in.readObject();
        name = (String) in.readObject();
        productId = (Integer) in.readObject();
        //
       // bitmap = BitmapFactory.decodeResource(getResources(), R.id.imgPreview);
        // Convert it to byte
       // ByteArrayOutputStream stream = new ByteArrayOutputStream();
       // Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(stream.toByteArray()));

        //

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int b;
        while ((b = in.read()) != -1)
            byteStream.write(b);
        byte bitmapBytes[] = byteStream.toByteArray();

        picture = BitmapFactory.decodeByteArray(bitmapBytes, 0,
                bitmapBytes.length);
    }
}

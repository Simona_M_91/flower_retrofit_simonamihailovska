package com.example.testwithretrofit.model.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testwithretrofit.R;
import com.example.testwithretrofit.model.pojo.Flower;
import com.example.testwithretrofit.ui.FlowerItem;

import java.util.List;

public class FlowerItemAdapter extends RecyclerView.Adapter<FlowerItemAdapter.ItemViewHolder>{

    List<FlowerItem> data;
    LayoutInflater inflater;
    ItemClickListener clickListener;

    public FlowerItemAdapter(List<FlowerItem> data, LayoutInflater inflater, ItemClickListener clickListener) {
        this.data = data;
        this.inflater = inflater;
        this.clickListener = clickListener;
    }



    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.recycler_view_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        FlowerItem flowerItem = data.get(position);
        holder.imageView.setImageResource(flowerItem.getImageView());
        holder.title.setText(flowerItem.getTitle());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView title;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.flowerImage);
            title = (TextView) itemView.findViewById(R.id.subtitle);
        }
    }
}


